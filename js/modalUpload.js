// modal functions for upload
const modalUpload = document.querySelector(".modal-upload");
const uploadTrigger = document.querySelector(".btn-upload");
const closeButtonUpload = document.querySelector(".close-button-upload");

function toggleModalUpload() {
  modalUpload.classList.toggle("show-modal");
}

function windowOnClick(event) {
  if (event.target === modalUpload) {
    toggleModalUpload();
  }
}

uploadTrigger.addEventListener("click", toggleModalUpload);
closeButtonUpload.addEventListener("click", toggleModalUpload);
window.addEventListener("click", windowOnClick);

